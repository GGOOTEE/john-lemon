﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class KeyController : MonoBehaviour
{

    public GameObject key1;
    public GameObject key2;
    public GameObject key3;
    public GameObject key4;

    public GameObject door;
    

   
    int hasKeys = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetAllKeys()
    {
        hasKeys = 0;
        key1.SetActive(true);
        key2.SetActive(true);
        key3.SetActive(true);
        key4.SetActive(true);
    }

    public void CheckAllKeys()
    {
        if (hasKeys == 4)
        {
            door.SetActive(false);
        }
    }

    public void TouchedKey1()
    {
        key1.SetActive(false);
        key2.SetActive(false);
        key3.SetActive(false);
        key4.SetActive(false);
        hasKeys += 1;

        CheckAllKeys();
    }

    public void PickedUpKey()
    {
        hasKeys += 1;

        CheckAllKeys();

    }
}
